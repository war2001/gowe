package gowe

import "encoding/xml"

// WxPayProfitSharingAddReceiver 添加分账接收方  https://pay.weixin.qq.com/wiki/doc/api/allocation.php?chapter=27_3&index=4
func WxPayProfitSharingAddReceiver(wxPayConfig IWxPayConfig, body *WxPayProfitSharingAddReceiverBody) (*WxPayReceiverResponse, error) {
	// 处理参数
	if body.ReceiversAddModel != nil {
		body.Receiver = jsonString(*body.ReceiversAddModel)
	}
	// 业务逻辑
	bytes, err := wxPayDoWeChat(wxPayConfig, "/pay/profitsharingaddreceiver", body, 0)
	if err != nil {
		return nil, err
	}
	// 结果校验
	if err = wxPayDoVerifySign(wxPayConfig, bytes, true); err != nil {
		return nil, err
	}
	res := &WxPayReceiverResponse{}
	// 解析返回值
	err = xml.Unmarshal(bytes, res)
	return res, err
}

// WxPayProfitSharingAddReceiverV3 服务商模式-添加分账接收方 https://pay.weixin.qq.com/wiki/doc/apiv3_partner/apis/chapter8_1_8.shtml
func WxPayProfitSharingAddReceiverV3(wxPayConfig IWxPayConfig, body *WxPayProfitSharingAddReceiverBodyV3) (wxw *WxPayReceiverResponse, ex error) {
	// 业务逻辑
	err := wxPayDoWeChatV3(wxPayConfig, "/pay/profitsharingaddreceiver", body, &wxw)
	if err != nil {
		return wxw, err
	}
	return wxw, err
}

// WxPayProfitSharingPostOne 请求单次分账 https://pay.weixin.qq.com/wiki/doc/api/allocation.php?chapter=27_1&index=1
func WxPayProfitSharingPostOne(wxPayConfig IWxPayConfig, body *WxPayProfitSharingReceiverPostBody) (*WxPayReceiverResponse, error) {
	// 处理参数
	if body.ReceiversPostModel != nil {
		body.Receiver = jsonString(*body.ReceiversPostModel)
	}
	// 业务逻辑
	bytes, err := wxPayDoWeChat(wxPayConfig, "/secapi/pay/profitsharing", body, 0)
	if err != nil {
		return nil, err
	}
	// 结果校验
	if err = wxPayDoVerifySign(wxPayConfig, bytes, true); err != nil {
		return nil, err
	}
	res := &WxPayReceiverResponse{}
	// 解析返回值
	err = xml.Unmarshal(bytes, res)
	return res, err
}

// WxPayProfitSharingAddReceiverBody 添加分账接收方的参数
type WxPayProfitSharingAddReceiverBody struct {
	Receiver string `json:"receiver"` // (必填) 分账接收方对象，json格式 查看接口json格式
	// 用于生成 WxPayReceiversPostModel
	ReceiversAddModel *WxPayReceiversAddModel `json:"-"`
}

// WxPayProfitSharingAddReceiverBodyV3 添加分账接收方的参数
type WxPayProfitSharingAddReceiverBodyV3 struct {
	SubMchid       string `json:"sub_mchid" form:"sub_mchid"`                     // 子商户号 微信支付分配的子商户号，即分账的出资商户号。
	Appid          string `json:"appid" form:"appid"`                             // 应用ID 微信分配的公众账号ID
	SubAppid       string `json:"sub_appid,omitempty" form:"sub_appid"`           // 子商户应用ID子 商户的公众账号ID，分账接收方类型包含PERSONAL_SUB_OPENID时必填
	Type           string `json:"type" form:"type"`                               // 分账接收方类型 MERCHANT_ID：商户号（mch_id或者sub_mch_id）PERSONAL_OPENID：个人openid
	Account        string `json:"account" form:"account"`                         // 分账接收方账号 类型是MERCHANT_ID时，是商户号（mch_id或者sub_mch_id）类型是PERSONAL_OPENID时，是个人openid
	Name           string `json:"name,omitempty" form:"name"`                     // 分账接收方全称 分账接收方类型是MERCHANT_ID时，是商户全称（必传），当商户是小微商户或个体户时，是开户人姓名 分账接收方类型是PERSONAL_OPENID时，是个人姓名（选传，传则校验）
	RelationType   string `json:"relationType" form:"relationType"`               // 与分账方的关系类型 子商户与接收方的关系。本字段值为枚举：SERVICE_PROVIDER：服务商,STORE：门店,STAFF：员工,STORE_OWNER：店主,PARTNER：合作伙伴,HEADQUARTER：总部,BRAND：品牌方,DISTRIBUTOR：分销商,USER：用户,SUPPLIER：供应商,CUSTOM：自定义
	CustomRelation string `json:"customRelation,omitempty" form:"customRelation"` // 自定义的分账关系 子商户与接收方具体的关系，本字段最多10个字。当字段relation_type的值为CUSTOM时，本字段必填当字段relation_type的值不为CUSTOM时，本字段无需填写
}

// WxPayReceiversAddModel 添加分账接收方 分账接收方的json model
type WxPayReceiversAddModel struct {
	Type           string `json:"type" form:"type"`                               // 分账接收方类型 MERCHANT_ID：商户号（mch_id或者sub_mch_id）PERSONAL_OPENID：个人openid
	Account        string `json:"account" form:"account"`                         // 分账接收方账号 类型是MERCHANT_ID时，是商户号（mch_id或者sub_mch_id）类型是PERSONAL_OPENID时，是个人openid
	Name           string `json:"name,omitempty" form:"name"`                     // 分账接收方全称 分账接收方类型是MERCHANT_ID时，是商户全称（必传），当商户是小微商户或个体户时，是开户人姓名 分账接收方类型是PERSONAL_OPENID时，是个人姓名（选传，传则校验）
	RelationType   string `json:"relationType" form:"relationType"`               // 与分账方的关系类型 子商户与接收方的关系。本字段值为枚举：SERVICE_PROVIDER：服务商,STORE：门店,STAFF：员工,STORE_OWNER：店主,PARTNER：合作伙伴,HEADQUARTER：总部,BRAND：品牌方,DISTRIBUTOR：分销商,USER：用户,SUPPLIER：供应商,CUSTOM：自定义
	CustomRelation string `json:"customRelation,omitempty" form:"customRelation"` // 自定义的分账关系 子商户与接收方具体的关系，本字段最多10个字。当字段relation_type的值为CUSTOM时，本字段必填当字段relation_type的值不为CUSTOM时，本字段无需填写
}

// WxPayProfitSharingReceiverPostBody 请求单次分账的参数
type WxPayProfitSharingReceiverPostBody struct {
	TransactionId string `xml:"transaction_id"` // 微信支付订单号
	OutTradeNo    string `json:"out_trade_no"`  // 商户系统内部订单号,要求32个字符内,只能是数字、大小写字母_-|*且在同一个商户号下唯一.详见商户订单号
	Receiver      string `json:"receiver"`      // (必填) 分账接收方对象，json格式 查看接口json格式
	// 用于生成 WxPayReceiversPostModel
	ReceiversPostModel *WxPayReceiversPostModel `json:"-"`
}

// WxPayReceiversPostModel 请求单次分账 分账接收方列表的json model
type WxPayReceiversPostModel struct {
	Type        string `json:"type" form:"type"`               // 分账接收方类型 MERCHANT_ID：商户号（mch_id或者sub_mch_id）PERSONAL_OPENID：个人openid
	Account     string `json:"account" form:"account"`         // 分账接收方账号 类型是MERCHANT_ID时，是商户号（mch_id或者sub_mch_id）类型是PERSONAL_OPENID时，是个人openid
	Amount      int    `json:"amount" form:"amount"`           // 分账金额 分账金额，单位为分，只能为整数，不能超过原订单支付金额及最大分账比例金额
	Description string `json:"description" form:"description"` // 分账描述 分账的原因描述，分账账单中需要体现
	Name        string `json:"name,omitempty" form:"name"`     // 分账个人接收方姓名 可选项，在接收方类型为个人的时可选填，若有值，会检查与 name 是否实名匹配，不匹配会拒绝分账请求	1、分账接收方类型是PERSONAL_OPENID时，是个人姓名（选传，传则校验）
}

// WxPayReceiversResponseModel 请求分账返回结果列 分账接收方的json model
type WxPayReceiversResponseModel struct {
	Type            string `json:"type" form:"type"`                         // 分账接收方类型 MERCHANT_ID：商户号（mch_id或者sub_mch_id）PERSONAL_OPENID：个人openid
	Account         string `json:"account" form:"account"`                   // 分账接收方账号 类型是MERCHANT_ID时，是商户号（mch_id或者sub_mch_id）类型是PERSONAL_OPENID时，是个人openid
	Amount          int    `json:"amount" form:"amount"`                     // 分账金额 分账金额，单位为分，只能为整数，不能超过原订单支付金额及最大分账比例金额
	Name            string `json:"name" form:"name"`                         // 分账接收方全称 分账接收方类型是MERCHANT_ID时，是商户全称（必传），当商户是小微商户或个体户时，是开户人姓名 分账接收方类型是PERSONAL_OPENID时，是个人姓名（选传，传则校验）
	RelationType    string `json:"relationType" form:"relationType"`         // 与分账方的关系类型 子商户与接收方的关系。本字段值为枚举：SERVICE_PROVIDER：服务商,STORE：门店,STAFF：员工,STORE_OWNER：店主,PARTNER：合作伙伴,HEADQUARTER：总部,BRAND：品牌方,DISTRIBUTOR：分销商,USER：用户,SUPPLIER：供应商,CUSTOM：自定义
	CustomRelation  string `json:"customRelation" form:"customRelation"`     // 自定义的分账关系 子商户与接收方具体的关系，本字段最多10个字。当字段relation_type的值为CUSTOM时，本字段必填当字段relation_type的值不为CUSTOM时，本字段无需填写
	Description     string `json:"description" form:"description"`           // 分账描述 分账的原因描述，分账账单中需要体现
	FailReason      string `json:"fail_reason" form:"fail_reason"`           // 分账失败原因 分账失败原因，当分账结果result为CLOSED（已关闭）时，返回该字段 枚举值：1、ACCOUNT_ABNORMAL : 分账接收账户异常2、NO_RELATION : 分账关系已解除3、RECEIVER_HIGH_RISK : 高风险接收方4、RECEIVER_REAL_NAME_NOT_VERIFIED : 接收方未实名5、NO_AUTH : 分账权限已解除6、RECEIVER_RECEIPT_LIMIT : 接收方已达收款限额7、PAYER_ACCOUNT_ABNORMAL : 分出方账户异常
	DetailId        string `json:"detail_id" form:"detail_id"`               // 分账明细单号 微信分账明细单号，每笔分账业务执行的明细单号，可与资金账单对账使用
	FinishTime      string `json:"finish_time" form:"finish_time"`           // 完成时间 分账完成时间，遵循RFC3339标准格式，格式为 YYYY-MM-DDTHH:mm:ss.sss+TIMEZONE，YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss.sss表示时分秒毫秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。例如：2015-05-20T13:29:35:120+08:00表示北京时间2015年05月20日13点29分35秒。
	ReceiverAccount string `json:"receiver_account" form:"receiver_account"` // 分账接收方账号 分账接收方账号：类型是MERCHANT_ID时，是商户号（mch_id或者sub_mch_id）类型是PERSONAL_OPENID时，是个人openid
	ReceiverMchid   string `json:"receiver_mchid" form:"receiver_account"`   // 分账接收商户号 仅分账接收方类型为MERCHANT_ID时，填写微信支付分配的商户号
	Result          string `json:"result" form:"result"`                     // 分账结果 分账结果，枚举值：PENDING：待分账 SUCCESS：分账成功 CLOSED：分账失败已关闭
}

// WxPayReceiverResponse 添加分账返回
type WxPayReceiverResponse struct {
	WxResponseModel
	// 当return_code为SUCCESS时
	WxPayPartnerResponseModel
	Receiver      string `xml:"receiver"`       // 分账接收方，分账接收方对象（不包含分账接收方全称），json格式
	TransactionId string `xml:"transaction_id"` // 微信订单号 微信支付订单号
	OutOrderNo    string `xml:"out_order_no"`   // 商户分账单号 调用接口提供的商户系统内部的分账单号
	OrderId       string `xml:"order_id"`       // 微信分账单号 微信分账单号，微信支付系统返回的唯一标识
	Status        string `xml:"status"`         // 分账单状态 分账单状态，枚举值：PROCESSING：处理中,FINISHED：处理完成
	// ReceiversResponse 分账返回结果
	ReceiversResponse []WxPayReceiversResponseModel `xml:"-"`
}
